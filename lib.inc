section .text

%define LF_ASCII 0x0A
%define SPACE_ASCII 0x020
%define HT_ASCII 0x09
%define EOT_ASCII 0x04
%define MINUS_ASCII 0x02d
%define ASCII_0 0x030
%define ASCII_9 0x039
%define ASCII_DIGIT_SHIFT 0x030
 

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LF_ASCII
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp ; save stack pointer
    mov rax, rdi
    mov rcx, 10
    push 0
    .loop:
        cmp rax, 10
        jb .end
        xor rdx, rdx
        div rcx
        add rdx, ASCII_DIGIT_SHIFT ; digit to ascii
        dec rsp
        mov [rsp], dl
        jmp .loop
    .end:
        add rax, ASCII_DIGIT_SHIFT ; last digit to ascii
        dec rsp
        mov [rsp], al
    
    mov rdi, rsp
    call print_string
    mov rsp, r8 ; restore stack pointer
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print_unsigned_part
    push rdi
    mov rdi, MINUS_ASCII
    call print_char
    pop rdi
    neg rdi

    .print_unsigned_part:
        call print_uint
    
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - pointer 1
; rsi - pointer 2
string_equals:
    mov rax, 1
    xor rcx, rcx
    .loop:
        mov r8b, byte[rdi+rcx]
        mov r9b, byte[rsi+rcx]
        cmp r8b, r9b
        jne .not_equals
        cmp byte[rdi+rcx], 0
        je .end
        inc rcx
        jmp .loop

    .not_equals:
        xor rax, rax
    .end:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - buffer pointer
; rsi - buffer size

read_word:
    .whitespace_loop: ;skip whitespaces
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi

        cmp al, SPACE_ASCII
        je .whitespace_loop
        cmp al, HT_ASCII
        je .whitespace_loop
        cmp al, LF_ASCII
        je .whitespace_loop
        cmp al, EOT_ASCII
        je .whitespace_loop

    ; rax - first symbol of the word
    xor r8, r8
    .loop: ;read symbols
        test rax, rax
        je .end_of_loop
        cmp rax, SPACE_ASCII
        je .end_of_loop
        cmp rax, HT_ASCII
        je .end_of_loop
        cmp rax, LF_ASCII
        je .end_of_loop
        cmp rax, EOT_ASCII
        je .end_of_loop
        mov byte[rdi+r8], al
        inc r8
        cmp r8, rsi
        jge .buffer_overflow

        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi

        jmp .loop
    .end_of_loop:
        mov byte[rdi+r8], 0

    jmp .end
    .buffer_overflow:
        xor rdi, rdi
    
    .end:
        mov rax, rdi
        mov rdx, r8
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r8, r8
    xor rax, rax
    .digits_loop: ; read digits
        mov al, byte[rdi+r8]
        cmp al, ASCII_0
        jl .end_of_digits_loop
        cmp al, ASCII_9
        jg .end_of_digits_loop
        sub rax, ASCII_DIGIT_SHIFT ; from ascii
        push rax
        inc r8

        jmp .digits_loop
    .end_of_digits_loop:

    xor rdi, rdi ;result length
    xor rsi, rsi ;result
    mov r9, 10 ;base of 10 system
    mov r10, 1 ;digit multiplier
    .loop: ;calc result
        test r8, r8
        je .end
        pop r11
        dec r8
        inc rdi
        mov rax, r11
        mul r10
        add rsi, rax
        mov rax, r10
        mul r9
        mov r10, rax

        jmp .loop
    .end:
        mov rax, rsi
        mov rdx, rdi

    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov rsi, rdi ; old rdi value to check in past
    xor rax, rax
    mov al, byte[rdi]
    cmp al, MINUS_ASCII ; '-' ascii code
    jne .parse_unsigned
    inc rdi

    .parse_unsigned:
    push rdi
    push rsi
    call parse_uint
    pop rsi
    pop rdi

    test rdx, rdx
    je .end

    cmp rsi, rdi ; check on '-' before number
    je .end

    neg rax
    inc rdx

    .end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - string to copy pointer
; rsi - buffer pointer
; rdx - buffer size
string_copy:
    xor rax, rax
    xor r8, r8
    .loop:
        cmp r8, rdx
        jge .buffer_overflow
        mov al, byte[rdi+r8]
        mov byte[rsi+r8], al
        test al, al
        je .end
        inc r8
        jmp .loop

    .buffer_overflow:
        xor r8, r8

    .end:
        mov rax, r8

    ret
    